var globalConfig = {
  w:800,
  h:600,
  mode: "AUTO",
  scaleMode: "SHOW_ALL", //SHOW_ALL, EXACT_FIT
  i18n: {
    langs: ["en","es"],
    path: "assets/gui/i18n/",
    format: ".png",
    w: 163,
    h: 83
  },
  splash: { //The "Loading" page for your game
    loadingScreen: "assets/gui/LANG/splash.png", //splash background
    loadingBar: {
      fullBar: "assets/gui/LANG/loadingbar.png",
      position: {x:111,y:462}
    }
  },
  fonts: "assets/gui/fonts.css",
  guiConfig: "story/GUI.yaml",
  storySetup: "story/Setup.yaml",
  //as many story text files as you want
  storyText: [
        "story/LANG/YourStory.yaml"
    ],
}
